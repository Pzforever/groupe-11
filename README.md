Projet Grails Groupe11

Par :

- 08 ANDRIANINA Malala Minou
- 10 LOYENS Randy Heggy
- 17 RAFARAMALALA Andrianandrasana Ronaldo
- 45 RANDRIANOMENTSOA Tanjona Gael
- 55 RAZAFY Toavina Fanasina



Backend :

- La partie BACKEND est une couche où l’on manipule les données donc l’accès est sécurisé.
- Cette couche n’est accessible qu’aux utilisateurs ayant le rôle ADMIN et MODERATOR.
- on peut faire des CRUD (Create Read Update Delete) d’utilisateur et d’annonce
- La possibilité d’apporté des changements sur les entités dépendra du rôle de l’utilisateur qui se connecte.
  - Rôle ADMIN peut tout faire
  - Rôle MODERATOR peut tout modifier et voir mais ne peut rien créer ou supprimer
- Pour la création et la modification d’une annonce, une option d’Upload de fichier a été ajouter. (Changer le chemin sur path dans grails-app\conf\application.yml avec le chemin ou l’on veut mettre les photos uploader)
- Un système de pagination a été ajouter pour une bonne navigation sur les listes.
- Des barres de recherche ont été ajouter pour pouvoir filtrer les annonces
- Recherche Simple : remplir un seul champ : filtrer les annonces sur leurs titre / prix

API REST

-L’API est capable de renvoyer du JSON ou du XML en fonction des paramètres d’appel:
- API Annonce supporte les méthodes: GET, PUT, PATCH, DELETE
- API Annonces supporte les méthodes: GET, POST
- API User supporte les méthodes: GET, PUT, PATCH, DELETE
- API Users supporte les méthodes: GET, POST

-Une collection POSTMAN

  DEPLOIEMENT
- Le projet est déployé sur le plateforme d’hébergement Heroku

http://ancient-fjord-06776.herokuapp.com/projet/

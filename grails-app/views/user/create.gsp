<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="container-fluid">

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">
            <h1 class="h1 mb-0 text-gray-800" style="font-weight: bold">Utilisateur

            </h1>

        <li class="breadcrumb-item">
            <i class="fas fa-angle-right header-action" style="font-weight: bold;margin-top: 20px"></i>
        </li>
        <li class="breadcrumb-item">
            <g:link controller="user">
                <button  type="button" class="btn btn-success header-action"> <i class="fa fa-columns" aria-hidden="true"></i> Liste</button>
            </g:link>
        </li>
        <li class="breadcrumb-item">
            <g:link controller="user" action="create">
                <button type="button" class="btn btn-primary header-action"><i class="fa fa-plus" aria-hidden="true"></i> Création</button>
            </g:link>
        </li>
    </ol>
</div>
<div class="row">
    <div class="col-sm-9 col-md-7 col-lg-10 mx-auto">
        <div class="card card-signin my-5">
            <div class="card-body">
                <h5 class="card-title text-center">Création</h5>
                <g:if test='${flash.message}'>
                    <div class="alert alert-danger" role="alert">${flash.message}</div>
                </g:if>
                <form class="form-signin" action="register" method="POST" id="loginForm" autocomplete="off">
                    <div class="form-group">
                        <label for="role">Role</label>
                        <g:select class="form-control" name="role.id"
                                  from="${com.mbds.grails.Role.list()}"
                                  optionValue="authority"
                                  optionKey="id" />
                    </div>

                    <div class="form-group">
                        <label for="username">Identifiant</label>
                        <input type="text" placeholder="Identifiant" class="form-control" name="username" id="username" autocapitalize="none"/>
                    </div>

                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" placeholder="Mot de passe" class="form-control" name="password" id="password"/>
                    </div>

                    <div class="form-group">
                        <label for="password">Confirmation mot de passe</label>
                        <input type="password" placeholder="Confirmation" class="form-control" name="repassword" id="repassword"/>
                    </div>
                    <button id="submit" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Enregistrer</button>
                    <hr class="my-4">
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        document.forms['loginForm'].elements['username'].focus();
    });
</script>
</body>
</html>
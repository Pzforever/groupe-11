<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="container-fluid">

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">
                <h1 class="h1 mb-0 text-gray-800" style="font-weight: bold">Utilisateur

                </h1>

            <li class="breadcrumb-item">
                <i class="fas fa-angle-right header-action" style="font-weight: bold;margin-top: 20px"></i>
            </li>
            <li class="breadcrumb-item">
                <g:link controller="user">
                    <button  type="button" class="btn btn-success header-action"> <i class="fa fa-columns" aria-hidden="true"></i> Liste</button>
                </g:link>
            </li>
            <li class="breadcrumb-item">
                <g:link controller="user" action="create">
                    <button type="button" class="btn btn-primary header-action"><i class="fa fa-plus" aria-hidden="true"></i> Création</button>
                </g:link>
            </li>
        </ol>
    </div>
    <div class="col-md-10" style="margin-left: 100px">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Identifiant</th>
            <th scope="col">Role</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${userList}" var="user">
        <tr>
            <td>${user.username}</td>
            <td>${user.authorities.authority[0]}</td>
            <td>
            <g:link id = "${user.id}" action="edit" controller="user" class="btn btn-info btn-circle btn-sm" >
                <i class="fas fa-pencil-alt"></i>
            </g:link>
                <a href="#" onclick="getConfirmationDeleteUser(${user.id})" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></a></td>
        </tr>
        </g:each>
        </tbody>
    </table>
                <g:paginate next="Suivant" prev="Précédent"
                            maxsteps="0" controller="user"
                            action="index" total="${userCount}" />
            </div>
        </div>
    </div>
    </body>
</html>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Annonce G-11</title>
</head>
<body>
    <div id="content" role="main">
        <!-- Download Section Start -->
        <section id="download" class="section">
            <div class="container">
                <div class="section-header">

                    <h1 class="section-title wow fadeInDown" data-wow-duration="2000ms" data-wow-delay="0.3s"><span>Bienvenue dans votre éspace admin</span><g:link controller="annonce"><i class="fa fa-apple"></i>--></g:link></h1>
                </div>
            </div>
        </section>
        <!-- Download Section End -->
    </div>

</body>
</html>

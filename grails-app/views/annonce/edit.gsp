<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <style>
    * {box-sizing: border-box;}
    body {font-family: Verdana, sans-serif;}
    .mySlides {display: none;}
    img {vertical-align: middle;}

    /* Slideshow container */
    .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
    }

    /* Caption text */
    .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
    }

    /* Number text (1/3 etc) */
    .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* The dots/bullets/indicators */
    .dot {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
    }

    .active {
        background-color: #717171;
    }

    /* Fading animation */
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 1.5s;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @-webkit-keyframes fade {
        from {opacity: .4}
        to {opacity: 1}
    }

    @keyframes fade {
        from {opacity: .4}
        to {opacity: 1}
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 300px) {
        .text {font-size: 11px}
    }
    </style>
</head>

<body>

<div class="container-fluid">
<div id="edit-annonce" class="content scaffold-edit" role="main">
    <h1>Modification</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.annonce}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.annonce}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <%--<g:uploadForm controller="annonce" action="update" id="${annonce.id}">
        <g:hiddenField name="version" value="${this.annonce?.version}"/>
        <fieldset class="form">
            <div class="fieldcontain required">
                <label for="title">Title
                    <span class="required-indicator">*</span>
                </label>
                <input type="text" name="title" value="${annonce.title}" required="" id="title">
            </div>

            <div class="fieldcontain required">
                <label for="description">Description
                    <span class="required-indicator">*</span>
                </label>
                <input type="text" name="description" value="${annonce.description}" required=""
                               id="description">
            </div>

            <div class="fieldcontain required">
                <label for="price">Price
                    <span class="required-indicator">*</span>
                </label><input type="number decimal" name="price" value="${annonce.price}" required="" min="0.0" id="price">
            </div>

            <div class="fieldcontain">
                <label for="illustrations">Illustrations</label>
                <g:each in="${annonce.illustrations}" var="illustration">
                    <img src="${baseUrl + illustration.filename}" />
                </g:each>
            </div>

            <div class="fieldcontain">
                <label for="file">Upload</label>
                <input style="display: inline" type="file" name="file" id="file"/>
            </div>

            <div class="fieldcontain required">
                <label for="author">Author
                    <span class="required-indicator">*</span>
                </label>
                <g:select name="author.id" from="${userList}" optionKey="id" optionValue="username" />
            </div>
        </fieldset>
        <fieldset class="buttons">
            <input class="save" type="submit"
                   value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </fieldset>
    </g:uploadForm>--%>
    <div class="container">

        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">

            </div>

            <div class="col-md-12">
                <g:uploadForm class="form-signin" enctype="multipart/form-data" action="update" id="${annonce.id}" method="POST"
                        style="max-width:30rem !important;">
                    <br>

                    <div class="form-label-group">
                        <label class="m-0 font-weight-bold text-primary" for="title">Titre</label>
                        <input type="text" name="title" value="${this.annonce.title}" required="" id="title"
                               class="form-control"
                               autofocus="">
                    </div>
                    <br>

                    <div class="form-label-group">
                        <label class="m-0 font-weight-bold text-primary" for="description">Description</label>
                        <input type="text" name="description" value="${this.annonce.description}"
                               required="" id="description" class="form-control">

                    </div>
                    <br>

                    <div class="form-label-group">
                        <label class="m-0 font-weight-bold text-primary">Prix</label>
                        <input type="number" step="0.01" name="price" required="" min="0.0" id="price"
                               class="form-control" value="${this.annonce.price}">
                    </div>
                    <br>

                        <!-- Illustrations -->
                        <div class="card shadow lg-12">
                            <div class="card-header py-12">
                                <h6 class="m-0 font-weight-bold text-primary">Illustrations</h6>
                            </div>
                            <div class="slideshow-container">

                                <g:each var="illustration" in = "${this.annonce.illustrations}">
                                    <div class="mySlides">
                                        <asset:image src="${illustration.filename}" class="illustrationmodif" height="300px" alt="Annonces" />
                                        <a onclick="getConfirmation(${illustration.id})">
                                            <div class="delete"><span class="fa fa-trash" data-toggle="tooltip" data-original-title="Supprimer"></span></div>
                                        </a>
                                    </div>
                                </g:each>
                                <div class="text-center d-none d-md-inline">
                                    <g:link class="rounded-circle border-0" onclick="plusSlides(-1)"><</g:link>
                                    <g:link class="rounded-circle border-0" onclick="plusSlides(1)">></g:link>
                                </div>
                            </div>

                        </div>
                    <br>
                    <div class="form-label-group">
                        <label class="m-0 font-weight-bold text-primary" for="file">Autres : </label>
                        <input style="display: inline" type="file" name="file" id="file"/>
                    </div>
                    <br>
                    <div class="form-label-group">
                        <input class="btn btn-primary btn-warning btn-lg btn-block" type="submit"
                               value="Modifier"/>

                    </div>
                </g:uploadForm>
                <br><br><br><br><br><br><br>
            </div>
        </div>

    </div>
</div>
</div>
<script>
    var i = 0;

    function add() {
        i++;
    }

    function add_fields() {
        document.getElementById('wrapper').innerHTML += '<input type="file" name="filename' + i + '" value="" required="" id="filename" placeholder="Entrez nom de fichier"> <br>\r\n';
    }
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }
    showSlides();

    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        setTimeout(showSlides, 5000); // Change image every 5 seconds
    }
</script>

</body>
</html>

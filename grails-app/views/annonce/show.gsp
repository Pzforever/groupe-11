<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="row">

        <!-- Content Column -->
        <div class="col-lg-6 mb-4">

            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Details: ${this.annonce.title}</h6>
                </div>
                <div class="card-body">
                    <h4 class="small font-weight-bold">Titre</h4>
                        <p>${this.annonce.title}</p>
                    <h4 class="small font-weight-bold">Description</h4>
                    <p>${this.annonce.description}</p>
                    <h4 class="small font-weight-bold">Price</h4>
                    <p>${this.annonce.price}$</p>

                </div>
            </div>

            <!-- Color System -->
            <div class="row">
                <div class="col-lg-12 mb-4">
                    <div class="card bg-secondary text-center text-white shadow">
                        <div class="card-body">
                            Author
                            <div class="text-white-50 small">${this.annonce.author.username}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-success text-center text-white shadow">
                        <div class="card-body ">
                            <g:link action="edit" resource="${this.annonce}">EDIT  <span class="fa fa-edit" data-toggle="tooltip" data-original-title="Modifier"></g:link></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-danger text-center text-white shadow">

                        <div class="card-body">
                            <a onclick="getConfirmation(${annonce.id})">DELETE <span class="fa fa-trash" data-toggle="tooltip" data-original-title="Supprimer"></span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col-lg-6 mb-4">

            <!-- Illustrations -->
            <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Illustrations</h6>
            </div>
            <div class="slideshow-container">

                <g:each var="illustration" in = "${this.annonce.illustrations}" status="i">
                        <div class="mySlides">
                            <asset:image src="${illustration.filename}" class="illustration" />
                            <p class="author">${illustration.filename}</p>
                        </div>
                </g:each>

                <a class="prev" onclick="plusSlides(-1)">❮</a>
                <a class="next" onclick="plusSlides(1)">❯</a>

            </div>

            </div>

        </div>
    </div>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
        }
    </script>

    </body>
</html>

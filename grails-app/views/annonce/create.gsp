<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center"><g:message code="default.create.label" args="[entityName]" /></h5>
                    <g:if test="${flash.message}">
                        <div class="message" role="status">${flash.message}</div>
                    </g:if>
                    <g:hasErrors bean="${this.annonce}">
                        <div class="col-lg-12 mb-4">
                            <div class="card bg-danger text-white shadow">
                                <div class="card-body text-white-50 small">
                                    <g:eachError bean="${this.annonce}" var="error">
                                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                    </g:eachError>
                                </div>
                            </div>
                        </div>
                    </g:hasErrors>
                    <g:uploadForm controller="annonce" action="save" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="title">Titre</label>
                                <input type="text" placeholder="Titre de l'annonce" class="form-control" name="title" id="title" autocapitalize="none" required/>
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" placeholder="Description de l'annonce" class="form-control" name="description" id="description" required/>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="number" placeholder="Prix de l'annonce" class="form-control" name="price" id="price" min="50" required/>
                            </div>
                            <div class="form-group">
                                <label for="imageupload">Add illustration</label>
                                <fieldset class="form">
                                    <input type="file" name="imageupload" id="imageupload"/>
                                </fieldset>
                            </div>

                            <button id="submit" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Enregistrer</button>
                            <hr class="my-4">
                    </g:uploadForm>


                </div>
            </div>
        </div>
    </div>
    </body>
</html>

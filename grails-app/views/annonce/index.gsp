<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div class="container-fluid">

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item">
            <h1 class="h1 mb-0 text-gray-800" style="font-weight: bold">Annonce

            </h1>

        <li class="breadcrumb-item">
                <i class="fas fa-angle-right header-action" style="font-weight: bold;margin-top: 20px"></i>
        </li>
        <li class="breadcrumb-item">
            <g:link controller="annonce">
                <button  type="button" class="btn btn-success header-action"> <i class="fa fa-columns" aria-hidden="true"></i> Liste</button>
            </g:link>
        </li>
        <li class="breadcrumb-item">
            <g:link controller="annonce" action="create">
                <button type="button" class="btn btn-primary header-action"><i class="fa fa-plus" aria-hidden="true"></i> Création</button>
            </g:link>
        </li>
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0" action="index" method = "get">
            <div class="input-group header-action">
                <input class="form-control" type="text"  placeholder="Titre"  id="title"  value = "${title}" name="title" aria-label="Search" aria-describedby="basic-addon2" />
                <input class="form-control" type="text"  placeholder="Prix"  id="price"  value = "${price}" name="price" aria-label="Search" aria-describedby="basic-addon2" />
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
    </ol>
</div>
<div class="container-fluid">
    <g:each var="annonce" in = "${annonceList}" status="i">
        <div class="row row_annonce">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-md-10" style="margin-left: 100px">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-8 block_contenu">
                                <div class="text-xs font-weight-bold text-primary  mb-1">
                                    <g:link id = "${annonce.id}" action="show" controller="annonce">
                                        <div class="titre col-mr-7">
                                            <h3>${annonce.title}</h3>
                                        </div>
                                    </g:link>
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Description : <i class="valeur_annonce">${annonce.description}</i></div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Prix : <i class="valeur_annonce"><g:formatNumber number="${annonce.price}" format="###,##0.##" />$</i></div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Crée le :
                                    <i class="valeur_annonce">
                                        <g:formatDate format="dd MMMM yyyy" date="${annonce.dateCreated}"/> à
                                        <g:formatDate type="time" style="SHORT" date="${annonce.dateCreated}"/>
                                    </i></div>
                            </div>
                            <div class="col mr-2">

                                        <asset:image src="${annonce.illustrations[0].filename}" height="144px" class="illustrations" />
                            </div>
                        </div>
                        <div class="action">
                            <g:link id = "${annonce.id}" action="edit" controller="annonce">
                                <div class="edit">
                                    <span class="fa fa-edit" data-toggle="tooltip" data-original-title="Modifier"></span>
                                </div>
                            </g:link>
                            <g:link id = "${annonce.id}" action="show" controller="annonce">
                                <div class="detail">
                                    <span class="fa fa-eye" data-toggle="tooltip" data-original-title="Détail"></span>
                                </div>
                            </g:link>
                            <a onclick="getConfirmation(${annonce.id})">
                                <div class="delete"><span class="fa fa-trash" data-toggle="tooltip" data-original-title="Supprimer"></span></div>
                            </a>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <hr class="sidebar-divider my-0">
    </g:each>
</div>
<div class="mypagination">
    <p class="result">Résultat par page</p>
    <select onchange="pagination(this.value)" name="nbPage" id="nbPage" class ="nbParPage">
        <g:each var="lm" in="${listMax}" status="i">
            <g:if test="${lm == max}">
                <option selected value="${lm}">${lm}</option>
            </g:if>
            <g:else>
                <option value="${lm}">${lm}</option>
            </g:else>
        </g:each>
    </select>
    <g:paginate total="${countList ?: 0}" prev="&lt;" next="&gt;"/>

</div>

</body>
</html>
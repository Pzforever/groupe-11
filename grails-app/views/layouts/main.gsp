
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <asset:stylesheet src="vendor/fontawesome-free/css/all.min.css" type="text/css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



    <!-- Custom styles for this template-->
    <asset:stylesheet src="myStyle.css"/>


    <asset:stylesheet src="sb-admin-2.css"/>

    <asset:stylesheet src="sweetalert2.all.min.css"/>


    <g:layoutHead/>
</head>
<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <sec:ifLoggedIn>
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i><asset:image src="annonce.png" width="40px" heigth="40px"/></i>
                </div>
                <div class="sidebar-brand-text mx-3">Annonce G11   </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard
                <li class="nav-item active">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li> -->

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                FONCTIONNALITE
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                   aria-expanded="true" aria-controls="collapseTwo">

                    <i class="fa fa-columns" aria-hidden="true"></i>
                    <span>Annonce</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Gestion d'annonce</h6>
                        <g:link controller="annonce" action="create" class="collapse-item">

                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Création

                        </g:link>
                        <g:link controller="annonce" class="collapse-item">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            Liste
                        </g:link>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider">
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo2"
                   aria-expanded="true" aria-controls="collapseTwo">

                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span>Gestion Utilisateur</span>
                </a>
                <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Gestion d'utilisateur</h6>
                        <g:link controller="user" action="create" class="collapse-item">

                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Création

                        </g:link>
                        <g:link controller="user" class="collapse-item">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            Liste
                        </g:link>
                    </div>
                </div>
            </li>
            <!-- Nav Item - Utilities Collapse Menu -->

            <hr class="sidebar-divider">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
    </sec:ifLoggedIn>
        <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <!-- Topbar Navbar -->
            <sec:ifLoggedIn>
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><sec:loggedInUserInfo field='username'/></span>
                            <asset:image class="img-profile rounded-circle" src="undraw_profile.svg"/>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">

                            <g:link controller="user" class="dropdown-item" action="edit" id="${sec.loggedInUserInfo(field:'id')}">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </g:link>


                            <div class="dropdown-divider"></div>
                                <g:link controller="logout" class="dropdown-item deconnexion">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Déconnexion
                                </g:link>
                    </div>
                    </li>

                </ul>
            </sec:ifLoggedIn>
            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->

            <!-- contenu à includer-->
            <g:layoutBody></g:layoutBody>

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Groupe_11 BE-Website 2021</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->

</body>
<!-- Bootstrap core JavaScript-->
<asset:javascript src="jquery.min.js"/>
<asset:javascript src="sweetalert2.all.min.js"/>
<asset:javascript src="bootstrap.bundle.min.js"/>
<!-- Core plugin JavaScript-->
<asset:javascript src="jquery.easing.min.js"/>

<!-- Custom scripts for all pages-->
<asset:javascript src="sb-admin-2.min.js"/>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({
            placement : 'top'
        });
    });

    function getConfirmation (id){
        Swal.fire({
            title: 'Voulez vous vraiment supprimer cet annonce?',
            text: "L'action est irreversible",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Supprimer',
            cancelButtonText: 'Annuler'
        })
            .then((result) => {
                if(result.value){
                    $.ajax({
                        url: '<g:createLink controller = "annonce" action="delete" id="'+id+'"/>',
                        type: 'DELETE',
                        success: function(data){
                            if(data ==  "success"){
                                swal.fire("Annonce", "Suppression réussi", "success").
                                then((result) =>{
                                    window.location.reload();
                                });
                            }
                            else swal.fire("Annonce", "Suppression échoué", "error");
                        },
                        error: function(e){
                            swal.fire("Menu", "Accès non autorisé", "info");
                        }
                    });

                }
                else if (result.dismiss === Swal.DismissReason.cancel) {}

            });
    }
    function getConfirmationDeleteUser (id){
        Swal.fire({
            title: 'Voulez vous vraiment supprimer cet utilisateur?',
            text: "L'action est irreversible",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Supprimer',
            cancelButtonText: 'Annuler'
        })
            .then((result) => {
                if(result.value){
                    $.ajax({
                        url: '<g:createLink controller = "user" action="delete" id="'+id+'"/>',
                        type: 'DELETE',
                        success: function(data){
                            if(data ==  "success"){
                                swal.fire("Utilisateur", "Suppression réussi", "success").
                                then((result) =>{
                                    window.location.reload();
                                });
                            }
                            else swal.fire("Utilisateur", "Suppression échoué", "error");
                        },
                        error: function(e){
                            swal.fire("Menu", "Accès non autorisé", "info");
                        }
                    });

                }
                else if (result.dismiss === Swal.DismissReason.cancel) {}

            });
    }
    function pagination(value){
        var title = '${params.title}';
        var price = '${params.price}';
        // alert(window.location.href = '/menu/index?max='+value+'&nomMenu='+nom+'&prixMenu='+prix);
        var url  = ''
        if ((title == null || title == "") && (price == null || price == "")) url = '<g:createLink controller = "annonce" action="index" params="[max: value]"/>'
        else url = '<g:createLink controller = "annonce" action="index" params="[title: title, price: price,max: values]"/>'

        window.location.href = url+value
    }
</script>
</html>

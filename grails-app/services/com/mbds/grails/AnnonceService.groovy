package com.mbds.grails

import grails.gorm.services.Service

@Service(Annonce)
interface AnnonceService {

    Annonce get(Serializable id)

    List<Annonce> list(Map args)

    Long count()

    void delete(Serializable id)

    Annonce save(Annonce annonce)

    List<Annonce> findByPrice(String price,Map args)

    List<Annonce> findByPrice(String price)

    List<Annonce> findByTitleLike(String title,Map args)

    List<Annonce> findByTitleLike(String title)

    List<Annonce> findByTitleLikeAndPrice(String title,String price,Map args)

    List<Annonce> findByTitleLikeAndPrice(String title,String price)

}
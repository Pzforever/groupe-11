package com.mbds.grails

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

@Service(UserRole)
interface UserRoleService {

    UserRole get(Serializable id)

    List<UserRole> list(Map args)

    Long count()

    void delete(Serializable id)

    UserRole save(UserRole user)
}

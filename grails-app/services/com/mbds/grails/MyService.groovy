package com.mbds.grails

import grails.gorm.transactions.Transactional

@Transactional
class MyService {
    AnnonceService annonceService

    def listMax (){
        List<Integer> list = [5,10,15];
        return list;
    }

    def listAnnonce(String title, String price, params) {

        if ((title == null || title == "") && (price == null || price == "")) return annonceService.list(params)
        else{
            if(title != "" && price == "") return  Annonce.findAllByTitleLike('%'+title+'%', [max: params.max, offset: params.offset])
            else if(title == "" && price != "") return Annonce.findAllByPrice(price as Double,[max: params.max, offset: params.offset])
            else return Annonce.findAllByTitleLikeAndPrice('%'+title+'%',price as Double,[max: params.max, offset: params.offset])
        }
    }
    def countListAnnonce(title, price){
        if ((title == null || title == "") && (price == null || price == "")) return annonceService.list().size()
        else if(title!= "" && price == "") return annonceService.findByTitleLike('%'+title+'%').size()
        else if(title == "" && price != "") return annonceService.findByPrice(price).size()
        else return annonceService.findByTitleLikeAndPrice('%'+title+'%',price).size()
    }
}

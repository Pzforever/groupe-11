package com.mbds.grails

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*


class UserController {

    UserService userService
    UserRoleService userRoleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond userService.list(params), model: [userCount: userService.count(), username: params.username]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def show(Long id) {
        respond userService.get(id)
    }

    @Secured('ROLE_ADMIN')
    def create() {
        respond new User(params)
    }

    @Secured('ROLE_ADMIN')
    def save(User user) {
        if (user == null) {
            notFound()
            return
        }
        println "FOUN"
        try {
            userService.save(user)
        } catch (ValidationException e) {
            println "ERROR FIELD"
            respond user.errors, view: 'create'
            return
        }
        println "ERROR"
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def edit(Long id) {
        respond userService.get(id)
    }

    @Secured('ROLE_ADMIN')
    def update(User user) {
        if (user == null) {
            notFound()
            return
        }

        try {
            userService.save(user)
        } catch (ValidationException e) {
            respond user.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN')
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        try {
            def user = User.findById(id)
            if (user) {
                println user.username

                def delete = UserRole.removeAll(user)
                println delete
                if (delete != 0) {
                    userService.delete(id)
                }
            }
            render("success")
        }
        catch (ValidationException e) {
            render('echec')
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_MODO'])
    def register() {

        if (!params.password.equals(params.repassword)) {
            flash.message = "Le mot de passe et la confirmation mot de passe ne correspondent pas"
            if (params.id) {
                redirect(action: "edit", id: params.id)
            } else {
                redirect action: "create"
            }

            return
        } else {
            try {
                def user = null
                def role = null
                if (!params.id) {
                        user = new User(username: params.username, password: params.password)
                        userService.save(user)
                    role = Role.get(params.role.id)
                } else {
                    user = userService.get(params.id)
                    println user.username
                    user.username = params.username
                    user.password = params.password
                    role = Role.get(params.role.id)

                }

                if (user && role) {
                    if (!params.id) {
                        UserRole.create user, role
                    } else {
                        userService.save(user)
                        UserRole.removeAll(user)
                        UserRole.create user, role
                    }


                    UserRole.withSession {
                        it.flush()
                        it.clear()
                    }

                    flash.message = "Utilisateur enregistré avec succès."
                    redirect controller: "user", action: "index"
                } else {
                    flash.message = "Veuillez vérifier les champs"
                    if (params.id) {
                        redirect(action: "edit", id: params.id)
                    } else {
                        redirect action: "create"
                    }
                    return
                }
            } catch (ValidationException e) {
                flash.message = "Une erreur s'est produite"
                if (params.id) {
                    redirect(action: "edit", id: params.id)
                } else {
                    redirect action: "create"
                }
                return
            }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}

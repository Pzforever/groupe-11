package com.mbds.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.bind.annotation.CrossOrigin

import javax.servlet.http.HttpServletResponse

@Secured('ROLE_ADMIN')
@CrossOrigin(origins="*")
class ApiController {

    AnnonceService annonceService
    SpringSecurityService springSecurityService
    UserService userService

//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render annonceInstance as XML }
                    json { render annonceInstance as JSON }
                }
                serializeData(annonceInstance, request.getHeader("Accept"))
                break
            case "PUT":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                def annonceAsJson = request.getJSON()
                annonceInstance.title = annonceAsJson.title
                annonceInstance.description = annonceAsJson.description
                annonceInstance.price = tryParseToDouble(annonceAsJson.price.toString())
                annonceService.save(annonceInstance)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render annonceInstance as XML }
                    json { render annonceInstance as JSON }
                }
                serializeData(annonceInstance, request.getHeader("Accept"))
                break
            case "PATCH":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                def annonceAsJson = request.getJSON()
                if (annonceAsJson.title != null) {
                    annonceInstance.title = annonceAsJson.title
                }
                if (annonceAsJson.description != null) {
                    annonceInstance.description = annonceAsJson.description
                }
                if (annonceAsJson.price != null && annonceAsJson.price.toString() != "") {
                    annonceInstance.price = tryParseToDouble(annonceAsJson.price.toString())
                }
                annonceService.save(annonceInstance)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render annonceInstance as XML }
                    json { render annonceInstance as JSON }
                }
                serializeData(annonceInstance, request.getHeader("Accept"))
                break
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render annonceInstance as XML }
                    json { render annonceInstance as JSON }
                }
                annonceService.delete(params.id)
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def annonces() {
        switch (request.getMethod()) {
            case "POST":
                def connectedUser
                def userId
                if (springSecurityService.isLoggedIn()) {
                    connectedUser = springSecurityService.getCurrentUser()
                    userId = connectedUser.id
                }

                def user = User.get(userId)
                def annonceAsJson = request.getJSON()
                annonceAsJson.each {
                    annonce ->
                        def annonceInstance = new Annonce(
                                title: annonce.title,
                                description: annonce.description,
                                price: annonce.price
                        )
                        annonceInstance.addToIllustrations(new Illustration(filename: "new img.jpg"))
                        user.addToAnnonces(annonceInstance)
                }
                user.save(flush: true, failOnError: true)
                break
            case "GET":
                def annoncesInstance = Annonce.getAll()
                if (!annoncesInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render annoncesInstance as XML }
                    json { render annoncesInstance as JSON }
                }
                serializeData(annoncesInstance, request.getHeader("Accept"))
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
    }

///    GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                def listUsers = User.getAll()
                if (!listUsers)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render listUsers as XML }
                    json { render listUsers as JSON }
                }
                serializeData(listUsers, request.getHeader("Accept"))
                break
            case "POST":
                def usersInstance = request.getJSON()
                usersInstance.each {
                    user ->
                        def userInstance =new User(
                                username:user.username,
                                password:user.password,
                                enabled:true,
                                accountExpired:false,
                                accountLocked:false,
                                passwordExpired:false
                        ).save()

                }
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = 405
                break
        }
    }
    //    GET / PUT / PATCH / DELETE
    def user() {
        /*String id = params.id
        String username = params.username
        String password = params.password*/
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def user = User.get(params.id)
                if (!user)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render user as XML }
                    json { render user as JSON }
                }
                serializeData(user, request.getHeader("Accept"))
                break
            case "PUT":

                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def user = User.get(params.id)
                def usernam = params.username ? params.username : user.getUsername()
                def passwor = params.password ? params.password : user.getPassword()
                user.setUsername(usernam)
                if (params.password != "") {
                    user.setPassword(passwor)
                }
                if (!user)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                user.save(flush: true)
                response.withFormat {
                    xml { render user as XML }
                    json { render user as JSON }
                }
                serializeData(user, request.getHeader("Accept"))
                break
            case "PATCH":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                def userJson = request.getJSON()
                if(userJson.username!=null){
                    userInstance.username = userJson.username
                }
                if(userJson.password!=null){
                    userInstance.password = userJson.password
                }
                if(userJson.enabled!=null){
                    userInstance.enabled = userJson.enabled
                }
                if(userJson.accountExpired!=null){
                    userInstance.accountExpired = userJson.accountExpired
                }
                if(userJson.accountLocked!=null){
                    userInstance.accountLocked = userJson.accountLocked
                }
                if(userJson.passwordExpired!=null){
                    userInstance.passwordExpired = userJson.passwordExpired
                }

                userService.save(userInstance)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    xml { render userInstance as XML }
                    json { render userInstance as JSON }
                }
                serializeData(userInstance, request.getHeader("Accept"))
                break
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def user = User.get(params.id)
                if (!user)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                UserRole.removeAll(user)
                user.delete(flush: true)
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }

    }

    def serializeData(object, format) {
        switch (format) {
            case 'json':
            case 'application/json':
            case 'text/json':
                render object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                render object as XML
                break
        }
    }

    def tryParseToDouble(String value) {
        Double parsed = 0;
        try {
            parsed = Double.parseDouble(value);
        }
        catch (Exception) {
            parsed = 0;
        }
        return parsed;
    }
}

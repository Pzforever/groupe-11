package com.mbds.grails

import grails.plugin.springsecurity.annotation.Secured
import grails.util.Holders
import org.grails.datastore.mapping.validation.ValidationException

import static org.springframework.http.HttpStatus.*

class AnnonceController {

    AnnonceService annonceService
    MyService myService
    UserService userService

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def index(Integer max) {
        params.max = Math.min(max ?: 5, 100)
        def title  = params.title
        def price = params.price

        def listAnnonce = myService.listAnnonce(title,price,params)
        def countListAnnonce = myService.countListAnnonce(title,price)

        respond listAnnonce, model:[nbIllustrationAffiche: 4,listMax: myService.listMax(),max: params.max, countList: countListAnnonce, title: params.title, price: params.price]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def show(Long id) {
        respond annonceService.get(id)
    }

    @Secured('ROLE_ADMIN')
    def create() {
        respond new Annonce(params)
    }

    @Secured('ROLE_ADMIN')
    def save(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }

        try {

            def springSecurityService = Holders.applicationContext.springSecurityService
            def user = springSecurityService.currentUser
            //println user.id
            annonce.author = user

            //illustration
            def f = request.getFile("imageupload")
            if (!f.empty) {
                def fileName = f.originalFilename

               def chemin = grailsApplication.config.annonces.illustrations.path+'/'+annonce.title+'.jpg'
                def img=annonce.title+'.jpg'

                f.transferTo(new File(chemin))
                annonce.addToIllustrations(new Illustration(filename: "$img"))
            }
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def edit(Long id) {
        respond annonceService.get(id), model: [userList: User.list(), baseUrl: grailsApplication.config.annonces.illustrations.url]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def update() {
        def annonce = Annonce.get(params.id)
        annonce.title = params.title
        annonce.description = params.description
        annonce.price = Double.parseDouble(params.price)
        if (annonce == null) {
            notFound()
            return
        }
        try {
                    def f = request.getFile("file")
                    if (!f.empty) {
                        def chemin = grailsApplication.config.annonces.illustrations.path + '/' + annonce.title + '.jpg'
                        def img = annonce.title + '.jpg'
                        f.transferTo(new File(chemin))
                        annonce.addToIllustrations(new Illustration(filename: "$img"))
                    }
                    annonce.title = params.title
                    annonce.description = params.description
                    annonce.price = Float.parseFloat(params.price)

                    annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }


    @Secured('ROLE_ADMIN')
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        try{
            annonceService.delete(id)
            render ("success")
        }
        catch(ValidationException e){
            render('echec')
        }


        /* request.withFormat {
             form multipartForm {
                 flash.message = message(code: 'default.deleted.message', args: [message(code: 'annonce.label', default: 'Annonce'), id])
                 redirect action:"index", method:"GET"
             }
             '*'{ render status: NO_CONTENT }
         }*/
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
